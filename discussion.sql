-- [SECTION] Advance selects

-- Exlude records or NOT operator
SELECT FROM songs WHERE id != 3;

-- GREATER THAN OR EQUAL;
SELECT * FROM songs where id > 3;

SELECT * FROM songs WHERE id >= 3;

-- LESS THAN or EQUAL
SELECT * FROM songs WHERE id < 11;

SELECT * FROM songs WHERE id <= 11;

-- IN operator
SELECT * FROM songs WHERE id IN (1, 3, 11);

SELECT * FROM songs WHERE genre IN ("Pop", "KPOP");

--Partial Matches.
SELECT * FROM songs WHERE song_name LIKE "%a";

SELECT * FROM songs WHERE song_name LIKE "a%";

SELECT * FROM songs WHERE id LIKE "_1";

-- SORTING records
SELECT * FROM songs ORDER BY song_name;

SELECT * FROM songs ORDER BY song_name DESC;

-- Getting distinct record
SELECT DISTINCT genre FROM songs;

-- [INNNER JOIN]
SELECT albums.album_title, songs.song_name FROM albums JOIN songs ON albums.id = songs.album_id;
